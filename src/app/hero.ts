export class Hero {
  public sn!: number;
  public name!: string;
  public code!: string;
  public joinedDate!: string;
  public email!: string;
  public gender!: string;
  public address!: string;
  public position!: string;
  public mobile!: string;
}
