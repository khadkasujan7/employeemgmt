import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.scss']
})
export class HeroFormComponent {

  submitted: boolean = false;
  genders =['Male', 'Female', 'Others'];
  
  constructor(private fb: FormBuilder){}

  employeeForm = this.fb.group({
    sn: ['1',Validators.required],
    name: ['', Validators.required],
    code: ['',Validators.required],
    joinedDate: ['',Validators.required],
    email: ['',Validators.required],
    gender: ['male',Validators.required],
    address: ['',Validators.required],
    position: ['',Validators.required],
    mobile: ['',Validators.required]

  });

  get f()
  {
      return this.employeeForm.controls;
  }

  onSubmit(){
    console.log(this.employeeForm.value);
  }

  

  // employeeForm = new FormGroup({
  //   sn: new FormControl('1'), 
  //   name : new FormControl('Sujan Khadka'),
  //   code: new FormControl('Szan'), 
  //   joinedDate : new FormControl(''),
  //   email: new FormControl('khadkasujan7@gmail.vom'),
  //   gender: new FormControl('male'),
  //   address: new FormControl('123st Lokanthali'),
  //   position: new FormControl('Hero'),
  //   mobile : new FormControl('9818056580')
  // });


}
