import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { HeroFormComponent } from './hero-form/hero-form.component';

const routes: Routes = [

  { path: 'heroform', component: HeroFormComponent},
  { path: 'employeelist', component: EmployeeListComponent},
  { path: 'employee', loadChildren: ()=>import('./employee/employee.module').then(m=>m.EmployeeModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [EmployeeListComponent, HeroFormComponent]